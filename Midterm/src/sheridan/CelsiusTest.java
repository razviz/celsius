package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;
/**
 * 
 * @author Zain_Razvi
 *
 */
public class CelsiusTest {

	@Test
	public void testValidfromFahrenheitRegular() {
		int conversion = Celsius.fromFahrenheit(34);
		assertTrue("Invalid Celsius Conversion", conversion == 0);
	}
	@Test
	public void testValidfromFahrenheitException() {
		double conversion = Celsius.fromFahrenheit(-34);
		assertFalse("Invalid Celsius Conversion", conversion == -118);
	}
	@Test
	public void testValidfromFahrenheitBoundaryIn() {
		int conversion = Celsius.fromFahrenheit(32);
		assertTrue("Invalid Celsius Conversion", conversion == 0);
	}
	@Test
	public void testValidfromFahrenheitBoundaryOut() {
		int conversion = Celsius.fromFahrenheit(31);
		assertFalse("Invalid Celsius Conversion", conversion == -2);
	}

}
